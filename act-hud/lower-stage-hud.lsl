// lower-stage-hud.lsl - Control the ACT Lower Stage
// SPDX-License-Identifier: MIT

// Default listen channel
integer DEFAULT_CHANNEL = 30;

// Spew debug info
integer VERBOSE = FALSE;

// Memory limit in bytes
integer MEM_LIMIT = 32000;

// face 4 - Y rot 0.0

string TEXT = "";
integer CHANNEL;

list tab_offset = [0.375, 0.125, -0.125, -0.375];

integer BUTTON_LINK = 2;

list button_rot = [
    <0.0, 0.0, 0.0>,
    <0.0, 90.0, 0.0>,
    <0.0, 180.0, 0.0>,
    <0.0, 270.0, 0.0>
];

// ****************************************
// HUD Positioing

// HUD Positioning offsets
float bottom_offset = 1.36;
float left_offset = -0.22;
float right_offset = 0.22;
float top_offset = 0.46;
integer last_attach = 0;
// ****************************************

// Based on SplitLine() from http://wiki.secondlife.com/wiki/SplitLine
string SplitLine(string _source, string _separator) {
    integer offset = 0;
    integer separatorLen = llStringLength(_separator);
    integer split = -1;

    do {
        split = llSubStringIndex(llGetSubString(_source, offset, -1), _separator);
        if (split != -1) {
            _source = llGetSubString(_source, 0, offset + split - 1) + "\n" + llGetSubString(_source, offset + split + separatorLen, -1);
            offset += split + separatorLen;
        }
    } while (split != -1);
    return _source;    
}

// Parses the object description into globals
// <channel>[:debug]
// All values are set directly and not returned

parse_desc(string desc) {
    list _args = llParseString2List(desc, [ ":" ], []);

    // Set defaults
    CHANNEL = DEFAULT_CHANNEL;

    integer i = 0;
    string s = llList2String(_args, i);
    while (s != "") {
        log("arg: " + s);

        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);
        if (_key == "channel") {
            CHANNEL = (integer)_val;
        }

        // Look for boolean args
        else if (s == "debug") {
            VERBOSE = TRUE;
        }
        i++;
        s = llList2String(_args, i);
    }
}

rotate_button(integer side) {
    rotation rot = llEuler2Rot(llList2Vector(button_rot, side) * DEG_TO_RAD);
    llSetLinkPrimitiveParamsFast(
        BUTTON_LINK,
        [PRIM_ROT_LOCAL, rot]
    );
}

button(string name, integer link, integer face, vector p) {
    if (name == "button") {
        if (face == 4) {
            // handle 5x2 buttons
            integer bx = (integer)(p.x * 5);
            integer by = (integer)(p.y * 2);
            if (bx == 0 && by == 0) {
                send(5541, "down");
            }
            else if (bx == 0 && by == 1) {
                send(5541, "up");
            }
            else if (bx == 1 && by == 0) {
                send(5542, "down");
            }
            else if (bx == 1 && by == 1) {
                send(5542, "up");
            }
            else if (bx == 2 && by == 0) {
                send(5543, "down");
            }
            else if (bx == 2 && by == 1) {
                send(5543, "up");
            }
            else if (bx == 3 && by == 0) {
                send(5544, "down");
            }
            else if (bx == 3 && by == 1) {
                send(5544, "up");
            }
            else if (bx == 4 && by == 0) {
                send(5545, "down");
            }
            else if (bx == 4 && by == 1) {
                send(5545, "up");
            }
        }
    }
    else if (face == 4) {
        // handle 4x1 buttons
        integer bx = (integer)(p.x * 4);
        integer by = (integer)(p.y * 1);
        if (bx >= 0 && bx <= 3) {
            // rotate link 2
            rotate_button(bx);
            llOffsetTexture(0.0, llList2Float(tab_offset, bx), 4);
        }
    }
}

log(string msg) {
    if (VERBOSE == 1) {
        llOwnerSay(msg);
    }
}

send(integer channel, string message) {
    llRegionSay(channel, message);
    log(message);
}

// Reset HUD titles
reset() {
    // Initialize attach state
    last_attach = llGetAttached();
    log("state_entry() attached=" + (string)last_attach);

    parse_desc(llGetObjectDesc());
    integer i = llGetNumberOfPrims();
    for (; i >= 0; --i) {
        list p = llGetLinkPrimitiveParams(i, [PRIM_NAME, PRIM_DESC]);
        llSetLinkPrimitiveParamsFast(i, [
            PRIM_TEXT, SplitLine(llList2String(p, 1), "~"), <1,1,1>, 1
        ]);
    }
    // Don't show root prim text
//    llSetLinkPrimitiveParamsFast(LINK_ROOT, [
//        PRIM_TEXT, "", <1,1,1>, 1
//    ]);

    log("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        reset();
    }

    touch_start(integer total_number) {
        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        list p = llGetLinkPrimitiveParams(link, [PRIM_NAME, PRIM_DESC]);
        log("link="+(string)link+"  face="+(string)face+"  pos="+(string)pos);

        button(llGetLinkName(link), link, face, pos);
    }

// ****************************************
// HUD Positioing

    attach(key id) {
        integer attach_point = llGetAttached();
        if (id != NULL_KEY && attach_point > 0 && attach_point != last_attach) {

            // Nasty if else block
            if (attach_point == ATTACH_HUD_TOP_LEFT) {
                llSetPos(<0.0, left_offset, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_CENTER) {
                llSetPos(<0.0, 0.0, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_RIGHT) {
                llSetPos(<0.0, right_offset, top_offset>);
            }
//            else if (attach_point == ATTACH_HUD_CENTER_1) {
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
//            else if (attach_point == ATTACH_HUD_CENTER_2) {
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
            else if (attach_point == ATTACH_HUD_BOTTOM_LEFT) {
                llSetPos(<0.0, left_offset, bottom_offset>);
            }
            else if (attach_point == ATTACH_HUD_BOTTOM) {
                llSetPos(<0.0, 0.0, bottom_offset>);
            }
            else if (attach_point == ATTACH_HUD_BOTTOM_RIGHT) {
                llSetPos(<0.0, right_offset, bottom_offset>);
            }
            last_attach = attach_point;
        }
    }
// ****************************************
}
