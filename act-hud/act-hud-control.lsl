// ACT HUD Control
//
// Based on tomjs master_button v7
// v1 - ACT HUD: add ACT comm and Title Menu
// v2 - Add channel prefix to text output
// v3 - Comm & cam updates
//
// Description field:
// channel - The channel to use for ACT comm

// The channel for inter-HUD communication
integer COMM_CHANNEL = 8;

// Shown in front of all text from another HUD, along with the channel number
// Set to empty string to turn off
string COMM_PREFIX = "> ";

// Spew log info
integer VERBOSE = FALSE;

// ****************************************
// Titler stuff

integer title_link;
integer TITLE_CHANNEL = -9898767;
integer title_handle;
list TITLE_DIALOG = [
    "On",
    "Off",
    "Set Title"
];
integer MENU_CHANNEL = -9898768;
integer menu_handle;

// ****************************************
// Comm stuff

integer comm_listen;
integer repeat_listen;
string comm_prefix;

tune_channel() {
    if (comm_listen)
        llListenRemove(comm_listen);
    if (repeat_listen)
        llListenRemove(repeat_listen);
 
    if (!COMM_CHANNEL) {
        comm_listen = 0;
        repeat_listen = 0;
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [
            PRIM_TEXT, "", <1,1,1>, 1
        ]);
        return;
    }
 
    comm_listen = llListen(COMM_CHANNEL, "", owner, "");
    repeat_listen = llListen(-COMM_CHANNEL, "", NULL_KEY, "");
    llOwnerSay("Listening on channel " + (string)COMM_CHANNEL);
    llSetLinkPrimitiveParamsFast(LINK_ROOT, [
        PRIM_TEXT, (string)COMM_CHANNEL, <1,1,1>, 1
    ]);
    if (COMM_PREFIX == "") {
        comm_prefix = "";
    } else {
        comm_prefix = (string)COMM_CHANNEL + COMM_PREFIX;
    }
}

// ****************************************
// Venue Stuff

list venues = [
    "Avilion Bard Circle", "Avilion", <234.00000, 185.00000, 1413.00000>,
    0
];

integer venue_index;
string venue_name;
string region_name;
vector center_pos;

// ****************************************
// Camera stuff

// stride 3 list <pos>, <rot-in-deg>, ??
list cam_slots = [
    // 1
    <234.00000, 158.00000, 1419.50000>, <-14.17500, 0.30000, 88.85000>, 0,
    // 2
    <212.40000, 195.90000, 1420.00000>, <19.50000, 20.00000, -46.00000>, 0,
    // 3
    <255.90000, 195.75000, 1417.75000>, <16.00000, -16.25000, -134.30000>, 0,
    // 4
    <234.00000, 176.00000, 1424.00000>, <51.00000, 1.35000, -89.00000>, 0,

    0
];

integer setcam = FALSE;
vector cam_pos;
vector cam_rot;
integer cam_is_set = FALSE;
float cam_alpha = 0.85;
vector cam_color_active = <0.00, 0.80, 0.00>;
vector cam_color_inactive = <0.40, 0.80, 1.00>;
integer cam_link;
integer camsel_link;
vector camsel_rot_in = <0.00, 0.00, 0.00>;
vector camsel_rot_out = <0.00, 90.00, 0.00>;
integer camsel_visible = FALSE;

// Cam select button
do_camsel() {
    rotation localRot = llList2Rot(llGetLinkPrimitiveParams(camsel_link, [PRIM_ROT_LOCAL]), 0);
    if (camsel_visible) {
        llSetLinkPrimitiveParamsFast(cam_link, [
            PRIM_COLOR, 4, cam_color_active, cam_alpha
        ]);
        llSetLinkPrimitiveParamsFast(camsel_link, [
            PRIM_ROT_LOCAL, llEuler2Rot(camsel_rot_in * DEG_TO_RAD)
        ]);
    } else {
        llSetLinkPrimitiveParamsFast(cam_link, [
            PRIM_COLOR, 4, cam_color_inactive, cam_alpha
        ]);
        llSetLinkPrimitiveParamsFast(camsel_link, [
            PRIM_ROT_LOCAL, llEuler2Rot(camsel_rot_out * DEG_TO_RAD)
        ]);
    }
}

// ****************************************
// HUD Positioing

// HUD Positioning offsets
float top_offset = -0.12;
float bot_offset = 0.02;
float left_offset = -0.03;
float right_offset = 0.20;
integer last_attach = 0;
integer hide_link;

do_hide(integer face) {
    if (face == 4) {
        rotation localRot = llList2Rot(llGetLinkPrimitiveParams(LINK_ROOT, [PRIM_ROT_LOCAL]), 0);
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [PRIM_ROT_LOCAL, llEuler2Rot(<0.0, 0.0, PI_BY_TWO>)*localRot]);
    } else {
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [PRIM_ROT_LOCAL, ZERO_ROTATION]);
    }
}

// ****************************************

// Memory limit in bytes
integer MEM_LIMIT = 34000;

key owner;


// ****************************************
// RLV

float LOCKED_ALPHA = 0.85;
vector LOCKED_COLOR = <0.8, 0.0, 0.0>;
float UNLOCKED_ALPHA = 0.85;
vector UNLOCKED_COLOR = <0.0, 0.8, 0.0>;

integer rlv_channel = 12345;
integer rlv_handle;
integer rlv_version = 0;
integer rlv_locked = FALSE;
integer rlv_button;
integer rlv_label;

init_rlv() {
    rlv_handle = llListen(rlv_channel, "", NULL_KEY, "");
    llSetTimerEvent(60.0);
    llOwnerSay("@versionnum="+(string)rlv_channel);
}

lockme() {
    if (rlv_version > 0) {
        llOwnerSay("@detach=n");
        rlv_locked = TRUE;
        llSetLinkPrimitiveParamsFast(rlv_button, [
            PRIM_COLOR, ALL_SIDES, LOCKED_COLOR, LOCKED_ALPHA
        ]);
        llSetLinkPrimitiveParamsFast(rlv_label, [
            PRIM_TEXT, "Unlock", <1,1,1>, 1
        ]);
    }
}

unlockme() {
    if (rlv_version > 0) {
        llOwnerSay("@detach=y");
        rlv_locked = FALSE;
        llSetLinkPrimitiveParamsFast(rlv_button, [
            PRIM_COLOR, ALL_SIDES, UNLOCKED_COLOR, UNLOCKED_ALPHA
        ]);
        llSetLinkPrimitiveParamsFast(rlv_label, [
            PRIM_TEXT, "Lock", <1,1,1>, 1
        ]);
    }
}

// ****************************************

// Based on SplitLine() from http://wiki.secondlife.com/wiki/SplitLine
string SplitLine(string _source, string _separator) {
    integer offset = 0;
    integer separatorLen = llStringLength(_separator);
    integer split = -1;

    do {
        split = llSubStringIndex(llGetSubString(_source, offset, -1), _separator);
        if (split != -1) {
            _source = llGetSubString(_source, 0, offset + split - 1) + "\n" + llGetSubString(_source, offset + split + separatorLen, -1);
            offset += split + separatorLen;
        }
    } while (split != -1);
    return _source;
}

// Parses the object description into globals
// [stagename=<name>][:label=<text>][:log]
// All values are set directly and not returned

parse_desc(string desc) {
    list _args = llParseString2List(desc, [ ":" ], []);

    integer i = 0;
    string s = llList2String(_args, i);
    while (s != "") {
        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);
        if (_key == "channel") {
            COMM_CHANNEL = (integer)_val;
        }

        // Look for boolean args
        else if (s == "log") {
            VERBOSE = TRUE;
        }
        i++;
        s = llList2String(_args, i);
    }
}

// ****************************************
// Venue Stuff

// Scan venues list the long way as there may be duplicate regions
// Uses globals: venues
// Sets globals: region_name, venue_index, venue_name, center_pos
integer find_venue() {
    integer i = 0;

    venue_index = -1;
    venue_name = "";
    region_name = llGetRegionName();
    integer len = llGetListLength(venues);
    do {
        if (llList2String(venues, i) == region_name) {
            // Check if we have a location match
            vector center = llList2Vector(venues, i+1);
            if (center == ZERO_VECTOR || llVecDist(center, llGetPos()) < 100) {
                venue_index = i - 1;
                venue_name = llList2String(venues, i-1);
                center_pos = center;
                // don't exit, there may be more than one region match
            }
        }
    } while (++i < len);
    return (venue_index >= 0);
}
// ****************************************

button(integer link, integer face, vector p, integer long) {
    string name = llGetLinkName(link);
    log("name: " + name);
    log("face: " + (string)face);
    log("pos: " + (string)p);

    // Root button
    if (name == "hide") {
        do_hide(face);
    }
    else if (name == "title") {
        // handle 1x1 buttons
        menu_handle = llListen(MENU_CHANNEL, "", owner, "");
        llSetTimerEvent(30.0);
        llDialog(
            llDetectedKey(0),
            "ACT HUD Titler",
            TITLE_DIALOG,
            MENU_CHANNEL
        );
    }
// ****************************************
// RLV Stuff
   else if (name == "lock") {
        if (long) {
            llResetScript();
        }
        else if (rlv_locked) {
            unlockme();
        } else {
            lockme();
        }
    }
// ****************************************
// Camera stuff
    else if (name == "cam") {
        // handle 1x1 buttons
        if (long) {
            // show cam position
            llRequestPermissions(owner, PERMISSION_TRACK_CAMERA);
        } else {
            camsel_visible = !camsel_visible;
            do_camsel();
            if (!camsel_visible && llGetAttached()) {
                // off
                llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
            }
        }
    }
    else if (name == "camsel" && llGetAttached() && venue_index >= 0) {
        // handle 5x1 buttons
        integer bx = (integer)(p.x * 5);
        integer by = (integer)(p.y * 1);
        if (bx == 0 && by == 0 && llGetAttached()) {
            // off
            cam_is_set = FALSE;
            llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
        }
        else if (bx >= 1 && bx <= 4 && by == 0) {
            bx--;
            cam_pos = llList2Vector(cam_slots, (venue_index * 4) + (bx * 3));
            cam_rot = llList2Vector(cam_slots, (venue_index * 4) + (bx * 3) + 1);
            cam_is_set = TRUE;
            llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
        }
    }
// ****************************************
}

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

send(integer channel, string message) {
    llRegionSay(channel, message);
    log((string)channel + ": " + message);
}

// Reset HUD titles
reset() {
    owner = llGetOwner();

// ****************************************
// Venue Stuff
    // Set region-specific bits
    find_venue();
    llOwnerSay("Venue: " + venue_name + ", " + region_name);
// ****************************************

    parse_desc(llGetObjectDesc());
    integer i = llGetNumberOfPrims();
    for (; i >= 0; --i) {
        string name;
        string label;
        list p = llGetLinkPrimitiveParams(i, [PRIM_NAME, PRIM_DESC]);
        name = llList2String(p, 0);
        if (i == 1) {
            // Do root button
        }
        else if (name == "hide") {
            hide_link = i;
        }
        else if (name == "title") {
            title_link = i;
        }
// ****************************************
// Camera Stuff
        else if (name == "cam") {
            cam_link = i;
        }
        else if (name == "camsel") {
            camsel_link = i;
        }
// ****************************************
// RLV Stuff
        else if (name == "lock") {
            // Set the button color
            llSetLinkPrimitiveParamsFast(i, [
                PRIM_COLOR, ALL_SIDES, UNLOCKED_COLOR, UNLOCKED_ALPHA
            ]);
            rlv_button = i;
        }
// ****************************************
        else {
            label = llList2String(p, 1);
        }
        llSetLinkPrimitiveParamsFast(i, [
            PRIM_TEXT, SplitLine(label, "~"), <1,1,1>, 1
        ]);
    }

// ****************************************
// Camera stuff
    do_camsel();
// ****************************************
// Comm stuff
    tune_channel();
// ****************************************

    do_hide(3); // 3 is the show button

    log("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        reset();
        init_rlv();
    }

    touch_start(integer num) {
        llResetTime();
    }

    touch_end(integer num) {
        integer long = (llGetTime() > 2.0);

        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        button(link, face, pos, long);
    }

    run_time_permissions(integer perm) {
// ****************************************
// Camera stuff
        if (perm & PERMISSION_CONTROL_CAMERA) {
            llClearCameraParams(); // reset camera to default
            if (cam_is_set) {
                llSetCameraParams([
                    CAMERA_ACTIVE, TRUE, // (TRUE or FALSE)
                    CAMERA_FOCUS, cam_pos+llRot2Fwd(llEuler2Rot(cam_rot * DEG_TO_RAD)), // camera rotation
                    CAMERA_FOCUS_LOCKED, TRUE, // (TRUE or FALSE)
                    CAMERA_POSITION, cam_pos, // region relative position
                    CAMERA_POSITION_LOCKED, TRUE // (TRUE or FALSE)
                ]);
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, 4, cam_color_active, cam_alpha
                ]);
            } else {
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, 4, cam_color_inactive, cam_alpha
                ]);
            }
        }
        if (perm & PERMISSION_TRACK_CAMERA) {
            llOwnerSay(venue_name+", "+region_name+"\ncam pos="+(string)llGetCameraPos()+"\ncam rot="+(string)(llRot2Euler(llGetCameraRot())*RAD_TO_DEG));
        }
// ****************************************
    }

// ****************************************
// HUD Positioing

    attach(key id) {
        integer attach_point = llGetAttached();
        if (id != NULL_KEY && attach_point > 0 && attach_point != last_attach) {

            // Nasty if else block
            if (attach_point == ATTACH_HUD_TOP_LEFT) {
//                log("attached: top left");
                llSetPos(<0.0, left_offset, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_CENTER) {
//                log("attached: top");
                llSetPos(<0.0, 0.0, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_RIGHT) {
//                log("attached: top right");
                llSetPos(<0.0, right_offset, top_offset>);
            }
//            else if (attach_point == ATTACH_HUD_CENTER_1) {
//                log("attached: center 1");
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
//            else if (attach_point == ATTACH_HUD_CENTER_2) {
//                log("attached: center 2");
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
            else if (attach_point == ATTACH_HUD_BOTTOM_LEFT) {
//                log("attached: bottom left");
                llSetPos(<0.0, left_offset, bot_offset>);
            }
            else if (attach_point == ATTACH_HUD_BOTTOM) {
//                log("attached: bottom");
                llSetPos(<0.0, 0.0, bot_offset>);
            }
            else if (attach_point == ATTACH_HUD_BOTTOM_RIGHT) {
//                log("attached: bottom right");
                llSetPos(<0.0, right_offset, bot_offset>);
            }
            last_attach = attach_point;
        }
    }
// ****************************************

    listen(integer channel, string name, key id, string message) {
        if (channel == MENU_CHANNEL) {
            llListenRemove(menu_handle);
            if (message == "Set Title") {
                llSetTimerEvent(30.0);
                title_handle = llListen(TITLE_CHANNEL, "", owner, "");
                llTextBox(owner, "Enter a new title", TITLE_CHANNEL);
            }
            else if (message == "On") {
                send(30, "|on");
            }
            else if (message == "Off") {
                send(30, "|off");
            }
        }
        else if (channel == TITLE_CHANNEL) {
            llListenRemove(title_handle);
            send(30, message);
            llOwnerSay("Setting title: \n" + message);
        }

// ****************************************
// RLV stuff
        else if (channel == rlv_channel) {
            llSetTimerEvent(0);
            llListenRemove(rlv_handle);

            log("RLV: " + message);
            rlv_version = (integer)message;
        }
 
// ****************************************
// Comm stuff
        else if (channel == COMM_CHANNEL) {
            // Repeat what we say on the COM channel
            string save_name = llGetObjectName();
            llSetObjectName(name);
            llOwnerSay(comm_prefix + message);
            llRegionSay(-COMM_CHANNEL, message);
            llSetObjectName(save_name);
        }
        else if (channel == -COMM_CHANNEL) {
            // Repeat what we hear others say on the com channel
            string save_name = llGetObjectName();
            llSetObjectName(name);
            llOwnerSay(comm_prefix + message);
            llSetObjectName(save_name);
        }
// ****************************************
   }

    timer() {
        llSetTimerEvent(0);
// ****************************************
// RLV stuff
        llListenRemove(rlv_handle);
// ****************************************
        llListenRemove(menu_handle);
        llListenRemove(title_handle);
    }

    changed(integer change) {
        if (change & (CHANGED_OWNER)) {
            llResetScript();
        }
        else if (change & (CHANGED_REGION | CHANGED_TELEPORT)) {
            reset();
        }
    }
}
