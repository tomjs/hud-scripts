// master button - detect touch
// SPDX-License-Identifier: GPL-3.0-or-later
// Layne Thomas (tomjs Resident)
//
// v3 - Add camera infrastructure, ~ in desc
// v4 - Add multi-venue curtain
// v5 - Set mem limits, improve camera settings
// v6 - Handle multiple venues in a sim
// v7 - Add RLV lock
// v8 - Simplify and cleanup RLV, Camera, Venue
// v9 - Add reset on long-click on lock, hard reset on sim crossing
// v10 - Merge lists for venues & cam data, add comm functions
// v11 - More reset, move rez/derez commands to top of script

// Stage rez/derez commands
// This is a simple chat command button, set the channel and the commands here

// This is what the Metaharpers Stage Tools expects, set StageName to, well, your stage name
integer STAGE_CHANNEL = 8;
string STAGE_REZ = "stagerez StageName";
string STAGE_DEREZ = "stagederez StageName";

// This is what the Spot-On Stage Manager expects, NotecardName is the exact name of the set notecard in the rezzer
//integer STAGE_CHANNEL = 42;
//string STAGE_REZ = "NotecardName";
//string STAGE_DEREZ = "clearstage";

// Root button label
string BUTTON_LABEL = "";

// Tipjar remote channel
integer TIPJAR_CHANNEL = 71;

// The channel for inter-HUD communication
integer COMM_CHANNEL = 6;

// Spew log info
integer VERBOSE = FALSE;

// ****************************************
// Venue Stuff

// Venue list: <name>, <region>, <reference-position>
integer venue_stride = 3;
list venues = [
    "Noir Neverland", "Tropic Breeze", <54.80000, 37.92000, 3185.73462>,
    "TerpsiCorp Pit", "TerpsiCorps Isle", <24.76779, 174.21861, 1794.30872>,
    "Queens of Burlesque", "TerpsiCorps Isle", <209.36530, 173.80542, 1793.23889>,
    "CandleWyck", "Chaillet", <85.00, 114.00, 25.00>,
    0
];

// stride 2 list: <channel>, <command-string>
list curtain_close = [
    // NN
    4242, "close",
    // Terps
    45, "show",
    // Queens of Burlesque
    51, "curtain",
    // CandleWyck
    999, "curtain",

    0
];

// stride 2 list: <channel>, <command-string>
list curtain_open = [
    // NN
    4242, "open",
    // Terps
    45, "hide",
    // Queens of Burlesque
    51, "curtain",
    // CandleWyck
    999, "curtain",

    0
];

integer venue_index;
string venue_name;
string region_name;
vector center_pos;

// ****************************************
// Camera stuff

// stride 3 list <pos>, <rot-in-deg>, ??
list cam_slots = [
    // NN
    // Front
    <91.00000, 38.00000, 3191.00000>, <0.00000, 0.00000, 180.00000>, 0,
    // Stage
    <62.00000, 38.00000, 3197.75000>, <0.00000, -50.00000, 180.00000>, 0,
    // Backstage
    <38.00000, 38.00000, 3191.00000>, <0.00000, 17.00000, 0.00000>, 0,
    // Tipjar
    <76.00000, 38.00000, 3185.00000>, <0.00000, 0.00000, 180.00000>, 0,

    // Terps Round
    // Front
    <41.00000, 174.00000, 1802.00000>, <0.00000, 0.00000, 180.00000>, 0,
    // Stage
    <26.00000, 174.00000, 1810.00000>, <0.00000, -72.00000, 180.00000>, 0,
    // Backstage
    <20.00000, 173.00000, 1791.00000>, <0.00000, 32.00000, 0.00000>, 0,
    // Tipjar
    <35.50000, 174.00000, 1798.00000>, <0.00000, 0.00000, 180.00000>, 0,

    // Queens of Burlesque
    // Front
    <181.50000, 174.00000, 1798.50000>, <0.00000, 11.00000, 0.00000>, 0,
    // Stage
    <203.00000, 174.00000, 1804.50000>, <0.00000, 58.00000, 0.00000>, 0,
    // Backstage
    <229.80000, 174.00000, 1799.00000>, <0.00000, -22.30000, 180.0000>, 0,
    // Tipjar
    <193.00000, 162.00000, 1797.00000>, <0.00000, 0.00000, 0.00000>, 0,

    // Candlewyck
    // Front
    <70.00000, 115.00000, 27.00000>, <0.00000, 11.00000, 0.00000>, 0,
    // Stage
    <83.00000, 115.00000, 34.00000>, <4.00000, 70.00000, -4.00000>, 0,
    // Backstage
    <105.82500, 107.70000, 28.30000>, <-13.34115, -7.49112, 118.79980>, 0,
    // Tipjar
    <55.00000, 115.00000, 30.50000>, <0.00000, 15.00000, -2.50000>, 0,

    0
];

vector cam_pos;
vector cam_rot;
integer cam_is_set = FALSE;
float cam_alpha = 0.85;
vector cam_color_active = <0.00, 0.80, 0.00>;
vector cam_color_inactive = <0.40, 0.80, 1.00>;
integer camsel_link;

// ****************************************
// Comm stuff

// Shown in front of all text from another HUD, along with the channel number
// Set to empty string to turn off
string COMM_PREFIX = "> ";
integer comm_listen;
integer repeat_listen;

tune_channel() {
    if (comm_listen)
        llListenRemove(comm_listen);
    if (repeat_listen)
        llListenRemove(repeat_listen);

    if (!COMM_CHANNEL) {
        comm_listen = 0;
        repeat_listen = 0;
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [
            PRIM_TEXT, "", <1,1,1>, 1
        ]);
        return;
    }

    comm_listen = llListen(COMM_CHANNEL, "", owner, "");
    repeat_listen = llListen(-COMM_CHANNEL, "", NULL_KEY, "");
    llOwnerSay("Listening on channel " + (string)COMM_CHANNEL);
    llSetLinkPrimitiveParamsFast(LINK_ROOT, [
        PRIM_TEXT, (string)COMM_CHANNEL, <1,1,1>, 1
    ]);
    if (COMM_PREFIX != "")
        COMM_PREFIX = (string)COMM_CHANNEL + COMM_PREFIX;
}

// ****************************************
// HUD Positioing

// HUD Positioning offsets
float top_offset = -0.12;
float bot_offset = 0.03;
float left_offset = -0.14;
float right_offset = 0.45;
integer last_attach = 0;

do_hide(integer face) {
    if (face == 4) {
        rotation localRot = llList2Rot(llGetLinkPrimitiveParams(LINK_ROOT, [PRIM_ROT_LOCAL]), 0);
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [PRIM_ROT_LOCAL, llEuler2Rot(<0.0, 0.0, PI_BY_TWO>)*localRot]);
    } else {
        llSetLinkPrimitiveParamsFast(LINK_ROOT, [PRIM_ROT_LOCAL, ZERO_ROTATION]);
    }
}
// ****************************************

// Memory limit in bytes
integer MEM_LIMIT = 32000;

key owner;


button(integer link, integer face, vector p, integer long) {
    string name = llGetLinkName(link);
    log("name: " + name);
    log("face: " + (string)face);
    log("pos: " + (string)p);

    // RESET button
    if (name == "reset") {
        llResetScript();
    }
    else if (name == "hide") {
        do_hide(face);
    }
// ****************************************
    else if (name == "b2") {
        // handle 1x2 buttons
        integer bx = (integer)(p.x * 1);
        integer by = (integer)(p.y * 2);
        if (bx == 0 && by == 0) {
//            send(8, "stopmove all");
        }
        else if (bx == 0 && by == 1) {
//            send(8, "moveto all w1");
        }
    }
    else if (name == "b3") {
        // handle 1x2 buttons
        integer bx = (integer)(p.x * 1);
        integer by = (integer)(p.y * 2);
        if (by == 0) {
//            send(8, "stopmove all");
        }
        else if (by == 1) {
//            send(8, "moveto all w1 4");
        }
    }
    else if (name == "b4") {
        // handle 1x2 buttons
        integer bx = (integer)(p.x * 1);
        integer by = (integer)(p.y * 2);
        if (bx == 0 && by == 0) {
        }
        else if (bx == 0 && by == 1) {
        }
    }
// ****************************************
// Stage Stuff
    else if (name == "stage") {
        // handle 1x2 buttons
        integer by = (integer)(p.y * 2);
        if (by == 0) {
            // Derez set
            send(STAGE_CHANNEL, STAGE_DEREZ);
        }
        else if (by == 1) {
            // Rez set
            send(STAGE_CHANNEL, STAGE_REZ);
        }
    }
// ****************************************
// RLV stuff
    else if (name == "lock") {
        if (long) {
            llResetScript();
        }
        else if (rlv_locked) {
            unlockme();
        } else {
            lockme();
        }
    }
// ****************************************
// Venue Stuff
    // Curtain
    else if (name == "curtain") {
        // handle 1x2 buttons
        integer by = (integer)(p.y * 2);
        if (by == 0) {
            send(
                llList2Integer(curtain_close, (venue_index * 2)),
                llList2String(curtain_close, (venue_index * 2) + 1)
            );
        }
        else if (by == 1) {
            send(
                llList2Integer(curtain_open, (venue_index * 2)),
                llList2String(curtain_open, (venue_index * 2) + 1)
            );
        }
    }
// ****************************************
// Camera stuff
    else if (name == "show_cam" || (name == "cam" && long)) {
        // show cam position
        llRequestPermissions(owner, PERMISSION_TRACK_CAMERA);
    }
    else if (name == "cam" && llGetAttached() && venue_index >= 0) {
        // handle 5x1 buttons
        integer bx = (integer)(p.x * 5);
        integer by = (integer)(p.y * 1);
        if (bx == 0 && by == 0) {
            // off
            cam_is_set = FALSE;
            llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
        }
        else if (bx >= 1 && bx <= 4 && by == 0) {
            bx--;
            // (venue_index * num_cam_buttons) + (button * stride of cam)
            cam_pos = llList2Vector(cam_slots, (venue_index * 4) + (bx * 3));
            cam_rot = llList2Vector(cam_slots, (venue_index * 4) + (bx * 3) + 1);
            cam_is_set = TRUE;
            llRequestPermissions(owner, PERMISSION_CONTROL_CAMERA);
        }
    }
// ****************************************
// Tipjar Stuff
    else if (name == "tipjar") {
        // handle 1x2 buttons
        integer by = (integer)(p.y * 2);
        if (by == 0) {
            send(TIPJAR_CHANNEL, "logout");
        }
        else if (by == 1) {
            send(TIPJAR_CHANNEL, "login");
        }
    }
// ****************************************
}

// ****************************************
// RLV

float LOCKED_ALPHA = 0.5;
vector LOCKED_COLOR = <1.0, 0.0, 0.0>;
float UNLOCKED_ALPHA = 0.5;
vector UNLOCKED_COLOR = <0.0, 1.0, 0.0>;

integer rlv_channel = 12345;
integer rlv_handle;
integer rlv_version = 0;
integer rlv_locked = FALSE;
integer rlv_button;

init_rlv() {
    rlv_handle = llListen(rlv_channel, "", NULL_KEY, "");
    llSetTimerEvent(60.0);
    llOwnerSay("@versionnum="+(string)rlv_channel);
}

lockme() {
    if (rlv_version > 0) {
        llOwnerSay("@detach=n");
        rlv_locked = TRUE;
        llSetLinkPrimitiveParamsFast(rlv_button, [
            PRIM_COLOR, ALL_SIDES, LOCKED_COLOR, LOCKED_ALPHA
        ]);
    }
}

unlockme() {
    if (rlv_version > 0) {
        llOwnerSay("@detach=y");
        rlv_locked = FALSE;
        llSetLinkPrimitiveParamsFast(rlv_button, [
            PRIM_COLOR, ALL_SIDES, UNLOCKED_COLOR, UNLOCKED_ALPHA
        ]);
    }
}

// ****************************************

// Based on SplitLine() from http://wiki.secondlife.com/wiki/SplitLine
string SplitLine(string _source, string _separator) {
    integer offset = 0;
    integer separatorLen = llStringLength(_separator);
    integer split = -1;

    do {
        split = llSubStringIndex(llGetSubString(_source, offset, -1), _separator);
        if (split != -1) {
            _source = llGetSubString(_source, 0, offset + split - 1) + "\n" + llGetSubString(_source, offset + split + separatorLen, -1);
            offset += split + separatorLen;
        }
    } while (split != -1);
    return _source;
}

// Parses the object description into globals
// [stagename=<name>][:label=<text>][:log]
// All values are set directly and not returned

parse_desc(string desc) {
    list _args = llParseString2List(desc, [ ":" ], []);

    integer i = 0;
    string s = llList2String(_args, i);
    while (s != "") {
        log("arg: " + s);

        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);
        if  (_key == "label") {
            BUTTON_LABEL = _val;
        }
        else if (_key == "channel") {
            COMM_CHANNEL = (integer)_val;
        }

        // Look for boolean args
        else if (s == "log") {
            VERBOSE = TRUE;
        }
        i++;
        s = llList2String(_args, i);
    }
}

// ****************************************
// Venue Stuff

// Scan venues list the long way as there may be duplicate regions
// Uses globals: venues
// Sets globals: region_name, venue_index, venue_name, center_pos
integer find_venue() {
    integer i = 0;

    venue_index = -1;
    venue_name = "";
    region_name = llGetRegionName();
    integer len = llGetListLength(venues);
    do {
        if (llList2String(venues, i + 1) == region_name) {
            // Check if we have a location match
            vector center = llList2Vector(venues, i + 2);
            if (center == ZERO_VECTOR || llVecDist(center, llGetPos()) < 100) {
                venue_index = i / venue_stride;
                venue_name = llList2String(venues, i);
                center_pos = center;
                // don't exit, there may be more than one region match
            }
        }
        i += venue_stride;
    } while (i < len);
    return (venue_index >= 0);
}

// ****************************************

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

send(integer channel, string message) {
    llRegionSay(channel, message);
    log((string)channel + ": " + message);
}

// Reset HUD titles
reset() {
    owner = llGetOwner();

// ****************************************
// Venue Stuff
    // Set region-specific bits
    find_venue();
    llOwnerSay("Venue: " + venue_name + ", " + region_name);
// ****************************************

    parse_desc(llGetObjectDesc());
    integer i = llGetNumberOfPrims();
    for (; i >= 0; --i) {
        string name;
        string label;
        list p = llGetLinkPrimitiveParams(i, [PRIM_NAME, PRIM_DESC]);
        name = llList2String(p, 0);
        if (i == 1) {
            // Do root button
            label = BUTTON_LABEL;
        }
// ****************************************
// Camera Stuff
        else if (name == "cam") {
            camsel_link = i;
            // Put the stage name on the root button
            label = " | " + venue_name + " | ";
        }
// ****************************************
// RLV Stuff
        else if (name == "lock") {
            // Set the button color
            llSetLinkPrimitiveParamsFast(i, [
                PRIM_COLOR, ALL_SIDES, UNLOCKED_COLOR, UNLOCKED_ALPHA
            ]);
            rlv_button = i;
            // label = STAGE_NAME;
        }
// ****************************************
        else {
            label = llList2String(p, 1);
        }
        llSetLinkPrimitiveParamsFast(i, [
            PRIM_TEXT, SplitLine(label, "~"), <1,1,1>, 1
        ]);
    }

// ****************************************
// Comm stuff
    tune_channel();
// ****************************************

    log("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        reset();
        init_rlv();
    }

    on_rez(integer num) {
        reset();
    }

    touch_start(integer num) {
        llResetTime();
    }

    touch_end(integer num) {
        integer long = (llGetTime() > 2.0);

        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        button(link, face, pos, long);
    }

    run_time_permissions(integer perm) {
//        log("r_t_p");
// ****************************************
// Camera stuff
        if (perm & PERMISSION_CONTROL_CAMERA) {
            llClearCameraParams(); // reset camera to default
            if (cam_is_set) {
                llSetCameraParams([
                    CAMERA_ACTIVE, TRUE, // (TRUE or FALSE)
                    CAMERA_FOCUS, cam_pos+llRot2Fwd(llEuler2Rot(cam_rot * DEG_TO_RAD)), // camera rotation
                    CAMERA_FOCUS_LOCKED, TRUE, // (TRUE or FALSE)
                    CAMERA_POSITION, cam_pos, // region relative position
                    CAMERA_POSITION_LOCKED, TRUE // (TRUE or FALSE)
                ]);
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, 4, cam_color_active, cam_alpha
                ]);
            } else {
                llSetLinkPrimitiveParamsFast(camsel_link, [
                    PRIM_COLOR, 4, cam_color_inactive, cam_alpha
                ]);
            }
        }
        if (perm & PERMISSION_TRACK_CAMERA) {
            llOwnerSay("\ncam pos="+(string)llGetCameraPos()+"\ncam rot="+(string)(llRot2Euler(llGetCameraRot())*RAD_TO_DEG));
        }
// ****************************************
    }

// ****************************************
// HUD Positioing

    attach(key id) {
        integer attach_point = llGetAttached();
        if (id != NULL_KEY && attach_point > 0 && attach_point != last_attach) {

            // Nasty if else block
            if (attach_point == ATTACH_HUD_TOP_LEFT) {
//                log("attached: top left");
                llSetPos(<0.0, left_offset, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_CENTER) {
//                log("attached: top");
                llSetPos(<0.0, 0.0, top_offset>);
            }
            else if (attach_point == ATTACH_HUD_TOP_RIGHT) {
//                log("attached: top right");
                llSetPos(<0.0, right_offset, top_offset>);
            }
//            else if (attach_point == ATTACH_HUD_CENTER_1) {
//                log("attached: center 1");
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
//            else if (attach_point == ATTACH_HUD_CENTER_2) {
//                log("attached: center 2");
//                llSetPos(<0.0, 0.0, 0.0>);
//            }
            else if (attach_point == ATTACH_HUD_BOTTOM_LEFT) {
//                log("attached: bottom left");
                llSetPos(<0.0, left_offset, bot_offset>);
            }
            else if (attach_point == ATTACH_HUD_BOTTOM) {
//                log("attached: bottom");
                llSetPos(<0.0, 0.0, bot_offset>);
            }
            else if (attach_point == ATTACH_HUD_BOTTOM_RIGHT) {
//                log("attached: bottom right");
                llSetPos(<0.0, right_offset, bot_offset>);
            }
            last_attach = attach_point;
        }
    }
// ****************************************

    listen(integer channel, string name, key id, string message) {
// ****************************************
// RLV
        if (channel == rlv_channel) {
            llSetTimerEvent(0);
            llListenRemove(rlv_handle);

            log("RLV: " + message);
            rlv_version = (integer)message;
        }
// ****************************************
// Comm stuff
        else if (channel == COMM_CHANNEL) {
            // Repeat what we say on the COMM channel
            string save_name = llGetObjectName();
            llSetObjectName(name);
            llOwnerSay(COMM_PREFIX + message);
            llRegionSay(-COMM_CHANNEL, message);
            llSetObjectName(save_name);
        }
        else if (channel == -COMM_CHANNEL) {
            // Repeat what we hear others say on the COMM channel
            string save_name = llGetObjectName();
            llSetObjectName(name);
            llOwnerSay(COMM_PREFIX + message);
            llSetObjectName(save_name);
        }
// ****************************************
    }

    timer() {
// ****************************************
// RLV
        llSetTimerEvent(0);
        llListenRemove(rlv_handle);
// ****************************************
    }

    changed(integer change) {
        if (change & (CHANGED_OWNER)) {
            llResetScript();
        }
        else if (change & (CHANGED_REGION | CHANGED_TELEPORT)) {
            reset();
        }
    }
}
