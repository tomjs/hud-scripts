// Product: NNNNNN Description
//
// HUD Listener - Listen for commands and do the work
// v1 - Initial release
// v2 - HUD handles resize menu
// v3 - Detect DEMO (last 4 chars of object name)
// v4 - Fix bug in resetting after transfer
// v5 - Fix bug in resize command
// v6 - Fix DEMO detection
// v2.0 - Add protocol version (major version is protocol)

// Commands:
//   set <texture>      Set a texture
//   debug on | off     Set logging
//   resize             Show the resize menu
//   scale <scale>      Change size by <scale> %

// Product ID - a unique string for each product
string PRODUCT = "111111";

// Default listen channel
integer CHANNEL = -11;
integer DEMO_CHANNEL = -12;

// The major version of the script is the protocol so all scripts with the same
// major version should be able to communicate
string PROTOCOL_VERSION = "2";

// Memory limit
integer MEM_LIMIT = 64000;

// Spew debug info
integer VERBOSE = FALSE;

string TEXT = "";
key ME;

integer doing_resize = FALSE;
float max_scale;
float min_scale;

float   cur_scale = 1.0;

list link_scales = [];
list link_positions = [];


log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

// Set textures sent by HUD
set_texture(string textures) {
    // input is one or mre lists of: link, face, texture-id
    log("set_texture(): " + textures);
    list l = llParseString2List(textures, ["|"], []);
    integer i;
    for (i=0;i<(integer)llGetListLength(l)/3;i++) {
        integer idx = (i * 3);
        integer link = llList2Integer(l, idx);
        integer face = llList2Integer(l, idx+1);
        string tex = llList2String(l, idx+2);
        llSetLinkTexture(link, tex, face);
        log("i="+(string)i+"  tex="+tex+"  face="+(string)face);
    }
}

// The resize code is based on http://wiki.secondlife.com/wiki/Linkset_resizer_with_menu

scanLinkset() {
    integer link_qty = llGetNumberOfPrims();
    integer link_idx;
    integer link_ofs = (link_qty != 1); // add 1 if more than one prim (as linksets start numbering with 1)
    list params;

    for (link_idx = 0; link_idx < link_qty; ++link_idx) {
        params = llGetLinkPrimitiveParams(link_idx + link_ofs, [PRIM_POS_LOCAL, PRIM_SIZE]);

        link_positions += llList2Vector(params, 0);
        link_scales    += llList2Vector(params, 1);
    }

    max_scale = llGetMaxScaleFactor() * 0.999999;
    min_scale = llGetMinScaleFactor() * 1.000001;
}

resizeObject(float scale) {
    integer link_qty = llGetNumberOfPrims();
    integer link_idx;
    integer link_ofs = (link_qty != 1);

    // scale the root
    llSetLinkPrimitiveParamsFast(link_ofs, [PRIM_SIZE, scale * llList2Vector(link_scales, 0)]);
    // scale all but the root
    for (link_idx = 1; link_idx < link_qty; link_idx++) {
        llSetLinkPrimitiveParamsFast(link_idx + link_ofs,
            [PRIM_SIZE,      scale * llList2Vector(link_scales, link_idx),
             PRIM_POS_LOCAL, scale * llList2Vector(link_positions, link_idx)]);
    }
}

// Reset script
reset() {
    if (llToLower(llGetSubString(llGetObjectName(), -4, -1)) == "demo") {
        CHANNEL = DEMO_CHANNEL;
    }
    ME = llGetOwner();
    llListen(CHANNEL, "", "", "");
    log("Listening on "+(string)CHANNEL);
}

default {
    attach(key id) {
        reset();
    }

    state_entry() {
        // Set up memory constraints
        llSetMemoryLimit(MEM_LIMIT);
        log("Free memory: " + (string)llGetFreeMemory() + "  Limit: " + (string)MEM_LIMIT);

        reset();
    }

    // Message format
    // <ver>|<product-id>:<cmd>
    listen(integer chan, string name, key id, string msg) {
        log("received="+msg);

        // Only listen to our own
        if (llGetOwnerKey(id) != ME) return;

        // Check protocol version
        integer ver_ix = llSubStringIndex(msg, "|");
        if (ver_ix < 0 || llGetSubString(msg, 0, ver_ix-1) != PROTOCOL_VERSION) {
            log("  ignoring, wrong protocol");
            return;
        }
        msg = llDeleteSubString(msg, 0, ver_ix);

        // Check product ID
        integer prodix = llSubStringIndex(msg, ":");
        if (prodix < 0 || llGetSubString(msg, 0, prodix-1) != PRODUCT) {
            log("  ignoring, wrong product ID");
            return;
        }
        msg = llDeleteSubString(msg, 0, prodix);

        log("cmd="+msg);

        // Parse first word to get the command
        string cmd = "";
        integer idx = llSubStringIndex(msg, " ");
        if (idx == -1) {
            cmd = msg;
        } else {
            cmd = llGetSubString(msg, 0, idx-1);
            msg = llDeleteSubString(msg, 0, idx);
        }
        cmd = llToLower(cmd);

        // Command handlers
        if (cmd == "set") {
            set_texture(msg);
        } else if (msg == "debug off") {
            VERBOSE = FALSE;
        } else if (msg == "debug on") {
            VERBOSE = TRUE;
        } else if (msg == "memory") {
            log("Free memory: " + (string)llGetFreeMemory() + "  Limit: " + (string)MEM_LIMIT);
        } else if (! doing_resize && msg == "resize") {
            if (! doing_resize) {
                log("scanning...");
                llOwnerSay("Resize Menu:\nMax scale: " + (string)max_scale + "\nMin scale: " + (string)min_scale + "\n\nCurrent scale: "+ (string)cur_scale);
                scanLinkset();
            }
            doing_resize = TRUE;
        } else if (cmd == "RESTORE") {
            cur_scale = 1.0;
        } else if (cmd == "MIN_SIZE") {
            cur_scale = min_scale;
        } else if (cmd == "MAX_SIZE") {
            cur_scale = max_scale;
        } else if (cmd == "DELETE") {
            llOwnerSay("Deleting resizer script...");
            llRemoveInventory(llGetScriptName());
            return; // prevents the menu from showing - llRemoveInventory is not instant
        } else if (cmd == "CLOSE") {
            doing_resize = FALSE;
        } else if (cmd == "scale") {
            cur_scale += (float)msg;
        }

        if (doing_resize) {
            //check that the scale doesn't go beyond the bounds
            if (cur_scale > max_scale) { cur_scale = max_scale; }
            if (cur_scale < min_scale) { cur_scale = min_scale; }

            resizeObject(cur_scale);
        }
    }

    changed(integer mask) {
        // Triggered when the object containing this script changes owner.
        if (mask & CHANGED_OWNER) {
            // Reset script to listen to the new owner
            llResetScript();
        }
    }

}
