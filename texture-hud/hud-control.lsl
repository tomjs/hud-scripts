// Product: NNNNNN Description
//
// HUD Control - Master HUD button pusher
// v1 - Initial release
// v2 - HUD handles resize menu
// v3 - Product ID, Detect DEMO (last 4 chars of object name)
// v4 - Fix bug in resetting after transfer, add debug mode
// v6 - Fix DEMO detection
// v2.0 - Add protocol version (major version is protocol)
//      - Add HUD positioning checks

// Product ID - a unique string for each product
string PRODUCT = "111111";

// How many textures are listed per product variant
integer num_faces = 2;

// List the textures
// link, face, texture-id
list textures = [
    // Variant 1
    0, 0, "00000000-0000-0000-0000-000000000000",
    0, 0, "00000000-0000-0000-0000-000000000000",

    // Variant 2
    0, 0, "00000000-0000-0000-0000-000000000000",
    0, 0, "00000000-0000-0000-0000-000000000000"
];

// Seconds before removing the menu listener
float DialogTimeout = 180;

// Default listen channel
integer CHANNEL = -11;
integer DEMO_CHANNEL = -12;

// The major version of the script is the protocol so all scripts with the same
// major version should be able to communicate
string PROTOCOL_VERSION = "2";

// Memory limit
integer MEM_LIMIT = 64000;

// Spew debug info
integer VERBOSE = FALSE;

// HUD Positioning offsets
float bottom_offset = 0.08;
float left_offset = 0.0;
float right_offset = 0.0;
float top_offset = -0.02;
integer last_attach = 0;

string TEXT = "";
key ME;
integer handle = 0;
integer menuChan;

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

vector get_size() {
    return llList2Vector(llGetPrimitiveParams([PRIM_SIZE]), 0);
}

adjust_pos() {
    integer current_attach = llGetAttached();

    // See if attachpoint has changed
    if ((current_attach > 0 && current_attach != last_attach) ||
            (last_attach == 0)) {
        vector size = get_size();

        // Nasty if else block
        if (current_attach == ATTACH_HUD_TOP_LEFT) {
            llSetPos(<0.0, left_offset - size.y / 2, top_offset - size.z / 2>);
        }
        else if (current_attach == ATTACH_HUD_TOP_CENTER) {
            llSetPos(<0.0, 0.0, top_offset - size.z / 2>);
        }
        else if (current_attach == ATTACH_HUD_TOP_RIGHT) {
            llSetPos(<0.0, right_offset + size.y / 2, top_offset - size.z / 2>);
        }
        else if (current_attach == ATTACH_HUD_BOTTOM_LEFT) {
            llSetPos(<0.0, left_offset - size.y / 2, bottom_offset + size.z / 2>);
        }
        else if (current_attach == ATTACH_HUD_BOTTOM) {
            llSetPos(<0.0, 0.0, bottom_offset + size.z / 2>);
        }
        else if (current_attach == ATTACH_HUD_BOTTOM_RIGHT) {
            llSetPos(<0.0, right_offset + size.y / 2, bottom_offset + size.z / 2>);
        }
        else if (current_attach == ATTACH_HUD_CENTER_1) {
        }
        else if (current_attach == ATTACH_HUD_CENTER_2) {
        }
        last_attach = current_attach;
    }
}

make_menu() {
    if (!handle) {
        menuChan = 500000 + (integer)llFrand(500000);
        handle = llListen(menuChan, "", llGetOwner(), "");
    }
    llSetTimerEvent(DialogTimeout);

    //the button values can be changed i.e. you can set a value like "-1.00" or "+2.00"
    //and it will work without changing anything else in the script
    llDialog(
        llGetOwner(),
        "menu",
        ["-0.05", "-0.10", "-0.25", "+0.05", "+0.10", "+0.25", /*"MIN_SIZE",*/ "RESTORE", /*"MAX_SIZE",*/ "CLOSE", "DELETE..."],
        menuChan
    );
}

// Send messages to the HUD listener
// <ver>|<product-id>:<cmd>
send(integer channel, string message) {
    if (VERBOSE) {
        llRegionSayTo(ME, channel, PRODUCT + ":debug on");
    }
    llRegionSayTo(ME, channel, PROTOCOL_VERSION + "|" + PRODUCT + ":" + message);
    log(message);
}

button(string name, integer face, vector p) {
    integer num = (integer)name;
    // The 3 is the stride of the texture list
    if (num > 0) {
        integer idx = ((num - 1) * (num_faces * 3));
        string tex_list = llDumpList2String(llList2List(textures, idx, idx+(num_faces*3)-1), "|");
        send(CHANNEL, "set " + tex_list);
    }
    else if (name == "resize") {
        send(CHANNEL, "resize");
        state resize;
    }
}

// Reset HUD
reset() {
    if (llToLower(llGetSubString(llGetObjectName(), -5, -1)) == "debug") {
        VERBOSE = TRUE;
    }
    if (llToLower(llGetSubString(llGetObjectName(), -4, -1)) == "demo") {
        CHANNEL = DEMO_CHANNEL;
    }
    ME = llGetOwner();
}

// Reset the resize state listener
reset_resize() {
    llListenRemove(handle);
    handle = 0;
    llSetTimerEvent(0);
}

default {
    attach(key id) {
        reset();
        if (id == NULL_KEY) {
            // Nothing to do on detach?
        } else {
            // Fix up our location
            adjust_pos();
        }
    }

    state_entry() {
        // Set up memory constraints
        llSetMemoryLimit(MEM_LIMIT);
        log("Free memory: " + (string)llGetFreeMemory() + "  Limit: " + (string)MEM_LIMIT);

        // Initialize attach state
        last_attach = llGetAttached();

        reset();
    }

    touch_start(integer total_number) {
        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        button(llGetLinkName(link), pos);
    }
}

state resize {
    state_entry() {
        make_menu();
    }

    touch_start(integer total_number) {
        integer link = llDetectedLinkNumber(0);
        integer face = llDetectedTouchFace(0);
        vector pos = llDetectedTouchST(0);

        button(llGetLinkName(link), face, pos);
    }

    timer() {
        send(CHANNEL, "CLOSE");
        reset_resize();
        state default;
    }

    listen(integer chan, string name, key id, string msg) {
        log("received="+msg);
        // Only listen to our own
        if (llGetOwnerKey(id) != ME) return;

        if (msg == "RESTORE" || msg == "MIN_SIZE" || msg == "MAX_SIZE" || msg == "DELETE") {
            send(CHANNEL, msg);
        } else if (msg == "DELETE...") {
            llDialog(llGetOwner(),"Are you sure you want to delete the resizer script?",
                ["DELETE","CANCEL"],menuChan);
            llSetTimerEvent(DialogTimeout);
            return;
        } else if (msg == "CANCEL") {
            // ignore but it will re-show the menu as it falls through
            state default;
        } else if (msg == "CLOSE") {
            send(CHANNEL, "CLOSE");
            llListenRemove(handle);
            handle = 0;
            llSetTimerEvent(0);
            state default;
            return; // prevents the menu from showing
        } else {
            send(CHANNEL, "scale " + msg);
        }

        make_menu();
    }

    changed(integer mask) {
        // Triggered when the object containing this script changes owner.
        if (mask & CHANGED_OWNER) {
            // Reset script to listen to the new owner
            llResetScript();
        }
    }
}
